package br.com.cplotter.sunmiprintervs1;

public interface PrinterCallBack {
    String getResult();
    void onReturnString(String result);
}
