package br.com.cplotter.sunmiprintervs1;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

//=============== CPLOTTER IMPORTS ==========
import android.app.Application;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.SystemClock;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;
import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.Locale;




/**
 * This class echoes a string called from JavaScript.
 */
public class SunmiPrinterVs1 extends CordovaPlugin {
    public aidlUtil aidl;
    @Override
    protected void pluginInitialize() {
      aidl=aidlUtil.getInstance();
      int x=0;
      do {
        aidl.connectPrinterService(webView.getContext());
        x++;
      }while (x<=3 && !aidl.isConnect());
    }
    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals("coolMethod")) {
            String message = args.getString(0);
            this.coolMethod(message, callbackContext);
            return true;
        }
        if(action.equals("toastMethod")){
            String message = args.getString(0);
            this.toastMethod(message,callbackContext);
            return true;
        }
        if(action.equals("printerTicket")){
            String message=args.getString(0);
            this.printerTicket(message,callbackContext);
            return true;
        }
      if(action.equals("printBar")){
        String message=args.getString(0);
        this.printBar(message,callbackContext);
        return true;
      }
      if(action.equals("startprinter")){
        this.startprinter(callbackContext);
        return true;
      }
      if(action.equals("printVoucher")){
        String message=args.getString(0);
        this.printVoucher(message,callbackContext);
        return true;
      }
      if(action.equals("printVoucherReport")){
        String message=args.getString(0);
        this.printVoucherReport(message,callbackContext);
        return true;
      }
      if(action.equals("abreCaixa")){
        String message=args.getString(0);
        this.abreCaixa(message,callbackContext);
        return true;
      }
      if(action.equals("sangria")){
        String message=args.getString(0);
        this.sangria(message,callbackContext);
        return true;
      }
      if(action.equals("saleReport")){
        String message=args.getString(0);
        this.saleReport(message,callbackContext);
        return true;
      }
      if(action.equals("caixaReport")){
        String message=args.getString(0);
        this.caixaReport(message,callbackContext);
        return true;
      }
        return false;
    }
    private void coolMethod(String message, CallbackContext callbackContext) {
        if (message != null && message.length() > 0) {
            callbackContext.success(message);
        } else {
            callbackContext.error("Expected one non-empty string argument.");
        }
    }
    private void toastMethod(String message, CallbackContext callbackContext){
        if(message == null){
            callbackContext.error("PARAMETRO INVÁLIDO");
        }else{
            Context context = webView.getContext();
            int duration = Toast.LENGTH_LONG;
            Toast toast = Toast.makeText(context, message, duration);
            toast.show();
        }
    }
    private void printerTicket(String jsonVendas,CallbackContext callbackContext){
        if(jsonVendas==null){
            callbackContext.error("Parametro Inválido");
        }else{
          try {

            if(!aidl.isConnect()){
              callbackContext.error("ERRO DE COMUNICAÇÃO COM A IMPRESSORA.");
              return;
            }
            //aidl.beginTransaction();
            JSONArray vendas = new JSONArray(jsonVendas);
            for (int i = 0; i < vendas.length(); i++) {
              JSONObject ticket = vendas.getJSONObject(i);
              aidl.printBitmap(this.getBitmap(ticket.getString("img")),aidl.AL_CENTRO);
              aidl.printText(ticket.getString("evento"),40,true,false,aidl.AL_CENTRO);
              aidl.printSeparator(aidlUtil.ESEPARADOR.SEPARADOR_1);
              JSONObject place = ticket.getJSONObject("place");
              aidl.printText(place.getString("name"),30,true,true,aidl.AL_CENTRO);
              aidl.printText(place.getString("street")+","+place.getString("number"),30,false,false,aidl.AL_CENTRO);
              JSONObject city = place.getJSONObject("city");
              JSONObject state=city.getJSONObject("state");
              String line=city.getString("name")+" - "+state.getString("abbr");
              aidl.printText(line,30,true,false,aidl.AL_CENTRO);
              Date dt=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(ticket.getString("data_evento"));
              DateFormat dtf=new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
              String sdt=dtf.format(dt);
              aidl.printText(sdt,30,true,false,aidl.AL_CENTRO);
              JSONObject tkt=ticket.getJSONObject("ticket");
              JSONObject sector=tkt.getJSONObject("sector");
              line=sector.getString("name");
              aidl.lineWrap(1);
              aidl.printText(line,50,true,false,aidl.AL_CENTRO);
              aidl.lineWrap(1);
              line=tkt.getString("gender");
              aidl.printText(line,40,true,false,aidl.AL_CENTRO);
              line=tkt.getString("lot_name");
              aidl.printText(line,30,false,false,aidl.AL_CENTRO);
              aidl.printSeparator(aidlUtil.ESEPARADOR.SEPARADOR_1);
              aidl.lineWrap(1);
              //QRCODE
              line=ticket.getString("key");
              aidl.printQr(line,8,3);
              aidl.printText(line,25,true,false,aidl.AL_CENTRO);
              aidl.printSeparator(aidlUtil.ESEPARADOR.SEPARADOR_1);
              Locale ptBr = new Locale("pt", "BR");
              double unitario=tkt.getDouble("price");
              String preco = NumberFormat.getCurrencyInstance(ptBr).format(unitario);
              line="Valor: "+preco;
              double taxp=tkt.getDouble("tax_percent");
              if(taxp>0){
                double tax= unitario*taxp/100;
                line +="\r\nTAXA: "+NumberFormat.getCurrencyInstance(ptBr).format(tax)+"\r\nTOTAL: "+NumberFormat.getCurrencyInstance(ptBr).format(unitario+tax);
              }
              aidl.printText(line,30,false,false,aidl.AL_CENTRO);
              dt=new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(ticket.getString("hora_venda"));
              line="Data Venda: "+dt.getDate()+"/"+(dt.getMonth()+1)+"/"+(dt.getYear()+1900)+" "+dt.getHours()+":"+dt.getMinutes();
              aidl.printText(line,20,false,false,aidl.AL_CENTRO);
              line="Term: "+ticket.getString("terminalPar")+" - REF: "+ticket.getString("terminalMaster");
              aidl.printText(line,20,false,false,aidl.AL_CENTRO);
              if(tkt.has("bilheteria") && tkt.getBoolean("bilheteria")){
                aidl.printText("BILHETERIA",25,true,false,aidl.AL_CENTRO);
              }
              aidl.printText(tkt.getString("bottom_info"),25,true,false,aidlUtil.AL_CENTRO);
              aidl.printSeparator(aidlUtil.ESEPARADOR.ATENCAO);
              aidl.lineWrap(5);
            }
            //aidl.endTransaction();
            callbackContext.success("OK");
          }catch (Exception ex){
            callbackContext.error(ex.getMessage());
          }
        }
    }
    private Bitmap getBitmap(String base64){
      if(base64==null || base64.length()<10)
        return aidl.getLogoDefault();
      try {
        byte[] decodedString = Base64.decode(base64, Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
      }catch (Exception ex){
        return  aidl.getLogoDefault();
      }

    }
    private void startprinter(CallbackContext callbackContext){
      aidl.startPrint();
      callbackContext.success("OK");
    }
    private void printBar(String jsonVendas,CallbackContext callbackContext){
        if(jsonVendas==null) {
          callbackContext.error("Parametro Inválido");
        }else{
          try{
            JSONObject vendas = new JSONObject(jsonVendas);
            Iterator<String> temp = vendas.keys();
            while (temp.hasNext()) {
              String key = temp.next();
              JSONObject prd=vendas.getJSONObject(key);

              for(int x=0;x<prd.getInt("qtd");x++){
                aidl.printText(prd.getString("name"),40,true,false,aidl.AL_CENTRO);
                aidl.printSeparator(aidlUtil.ESEPARADOR.SEPARADOR_1);
                aidl.printText(prd.getString("grupo"),25,false,false,aidl.AL_CENTRO);
                aidl.printText(prd.getString("produto"),40,true,false,aidl.AL_CENTRO);
                Locale ptBr = new Locale("pt", "BR");
                double unitario=prd.getDouble("preco_unitario");
                String preco = NumberFormat.getCurrencyInstance(ptBr).format(unitario);
                String line="Valor: "+preco;
                aidl.printText(line,25,false,false,aidl.AL_CENTRO);
                aidl.printSeparator(aidlUtil.ESEPARADOR.SEPARADOR_1);
                aidl.printQr(prd.getString("key"),6,3);
                aidl.printText(prd.getString("key"),25,false,false,aidl.AL_CENTRO);
                aidl.printSeparator(aidlUtil.ESEPARADOR.SEPARADOR_1);
                line="Term:"+prd.getString("terminal_partnet")+" REF:"+prd.getString("terminal_master");
                aidl.printText(line,20,false,false,aidl.AL_CENTRO);
                Date dt=new Date();
                DateFormat dtf=new SimpleDateFormat("dd/MM/yyyy HH:mm");
                String sdt=dtf.format(dt);
                line="Hora Emissão:"+sdt;
                aidl.printText(line,20,false,false,aidl.AL_CENTRO);
                aidl.printSeparator(aidlUtil.ESEPARADOR.VALIDO_DATA);
                aidl.lineWrap(5);
                callbackContext.success("OK");
              }

            }
          }catch(Exception ex){
            callbackContext.error(ex.getMessage());
          }
        }
    }
    private void printVoucher(String jsonVendas,CallbackContext callbackContext){
       if(jsonVendas==null) {
          callbackContext.error("Parametro Inválido");
        }else{
          try{
            JSONObject voucher = new JSONObject(jsonVendas);
            JSONObject event=voucher.getJSONObject("event");
            String ln=voucher.getString("id");
            String key="V"+String.format("%06d",Integer.parseInt(ln));
            ln=voucher.getString("id_event");
            ln=String.format("%06d",Integer.parseInt(ln));
            key+=ln;
            ln=voucher.getString("valor");
            ln=ln.replace(",","").replace(".","");
            ln=String.format("%010d",Integer.parseInt(ln));
            key+=ln;
            long time= System.currentTimeMillis();
            ln=String.valueOf(time);
            key+=ln;
            key+=";"+voucher.getString("terminal")+";"+voucher.getString("terminalMaster");
            aidl.printText("VOUCHER",80,true,false,aidl.AL_CENTRO);
            aidl.printBitmap(this.getBitmap(event.getString("img")),aidl.AL_CENTRO);
            aidl.printText(event.getString("name"),40,true,false,aidl.AL_CENTRO);
            aidl.printSeparator(aidlUtil.ESEPARADOR.SEPARADOR_1);
            aidl.printText(voucher.getString("identificacao"),25,false,false,aidlUtil.AL_CENTRO);
            Locale ptBr = new Locale("pt", "BR");
            double unitario=voucher.getDouble("valor");
            String preco = NumberFormat.getCurrencyInstance(ptBr).format(unitario);
            aidl.printText(preco,40,true,false,aidlUtil.AL_CENTRO);
            aidl.printSeparator(aidlUtil.ESEPARADOR.VOUCHER);
            aidl.lineWrap(1);
            aidl.printQr(key,6,3);
            aidl.printText(key,20,false,false,aidlUtil.AL_CENTRO);
            aidl.printSeparator(aidlUtil.ESEPARADOR.SEPARADOR_1);
            Date dt=new Date();
            DateFormat dtf=new SimpleDateFormat("dd/MM/yyyy HH:mm");
            String sdt=dtf.format(dt);
            ln="Hora Emissão:"+sdt;
            aidl.printText(ln,20,false,false,aidlUtil.AL_CENTRO);
            ln="Term: "+voucher.getString("terminal")+" REF: "+voucher.getString("terminalMaster");
            aidl.printText(ln,20,false,false,aidlUtil.AL_CENTRO);
            aidl.printSeparator(aidlUtil.ESEPARADOR.VALIDO_DATA);
            aidl.lineWrap(5);
            callbackContext.success(key);
          }catch(Exception ex){
            callbackContext.error(ex.getMessage());
          }
        }
    }
    private void printVoucherReport(String jsonReport,CallbackContext callbackContext) {
      if (jsonReport == null) {
        callbackContext.error("Parametro Inválido");
      } else {
        try {
          JSONObject report = new JSONObject(jsonReport);
          JSONArray vendas = report.getJSONArray("vendas");
          reportHeader(report.getString("terminal"),report.getString("terminal_master"));
          double total=0;
          Locale ptBr = new Locale("pt", "BR");
          for (int i=0; i < vendas.length(); i++) {
            JSONObject venda=vendas.getJSONObject(i);
            aidl.printText(venda.getString("nome"),30,true,false,aidlUtil.AL_ESQUERDA);
            double unitario=venda.getDouble("valor");
            String preco = NumberFormat.getCurrencyInstance(ptBr).format(unitario);
            double totalv=venda.getDouble("total");
            String totalStr = NumberFormat.getCurrencyInstance(ptBr).format(totalv);
            String ln=preco +" x ";
            ln+=venda.getString("quantidade")+" = "+totalStr;
            aidl.printText(ln,30,false,false,aidlUtil.AL_DIREITA);
            total+=totalv;
            aidl.printSeparator(aidlUtil.ESEPARADOR.SEPARADOR_2);
          }

          String preco = NumberFormat.getCurrencyInstance(ptBr).format(total);
          String ln="TOTAL: "+preco;
          aidl.printText(ln,35,true,false,aidlUtil.AL_DIREITA);
          aidl.lineWrap(5);
          callbackContext.success("ok");
        } catch (Exception ex) {
          callbackContext.error(ex.getMessage());
        }
      }
    }
    private void reportHeader(String termnal,String terminalMaster) throws Exception {
      aidl.printText("RELATÓRIO",40,true,true,aidlUtil.AL_CENTRO);
      String ln="Term.: "+termnal+" Ref.:"+terminalMaster;
      aidl.printText(ln,20,true,false,aidlUtil.AL_CENTRO);
      Date dt=new Date();
      DateFormat dtf=new SimpleDateFormat("dd/MM/yyyy HH:mm");
      String sdt=dtf.format(dt);
      ln="Hora Emissão: "+sdt;
      aidl.printText(ln,20,true,false,aidlUtil.AL_CENTRO);
      aidl.printSeparator(aidlUtil.ESEPARADOR.SEPARADOR_1);

    }
    private void abreCaixa(String movimento,CallbackContext callbackContext) {
      if (movimento == null) {
        callbackContext.error("Parametro Inválido");
      } else {
        try {
          JSONObject mov=new JSONObject(movimento);
          String via="Via do operador";
          Locale ptBr = new Locale("pt", "BR");
          for(int x=0;x<=1;x++){
            aidl.printText("ABETURA DE CAIXA",40,true,true,aidlUtil.AL_CENTRO);
            Date dt=new Date();
            DateFormat dtf=new SimpleDateFormat("dd/MM/yyyy HH:mm");
            String sdt=dtf.format(dt);
            aidl.printText("Data da abertura: "+sdt,20,true,false,aidlUtil.AL_CENTRO);
            sdt="Term.: "+mov.getString("terminal")+" Ref.: "+mov.getString("terminal_master");
            aidl.printText(sdt,20,true,false,aidlUtil.AL_CENTRO);
            sdt="Operador:\r\n"+mov.getString("operador");
            aidl.printText(sdt,30,true,false,aidlUtil.AL_CENTRO);
            aidl.printSeparator(aidlUtil.ESEPARADOR.SEPARADOR_1);
            aidl.printText(via,20,false,false,aidlUtil.AL_ESQUERDA);
            double unitario=mov.getDouble("valor");
            String preco = NumberFormat.getCurrencyInstance(ptBr).format(unitario);
            aidl.printText(preco,40,true,false,aidlUtil.AL_CENTRO);
            aidl.lineWrap(3);
            aidl.printText("____________________\r\nAss. Operador",20,false,false,aidlUtil.AL_CENTRO);
            via="Via do parceiro";
            aidl.lineWrap(3);
          }
          callbackContext.success("ok");
        } catch (Exception ex) {
          callbackContext.error(ex.getMessage());
        }
      }
    }
    private void sangria(String movimento,CallbackContext callbackContext) {
      if (movimento == null) {
        callbackContext.error("Parametro Inválido");
      } else {
        try {
          JSONObject mov=new JSONObject(movimento);
          String via="Via do operador";
          Locale ptBr = new Locale("pt", "BR");
          for(int x=0;x<=1;x++){
            aidl.printText("Sangria",40,true,true,aidlUtil.AL_CENTRO);
            Date dt=new Date();
            DateFormat dtf=new SimpleDateFormat("dd/MM/yyyy HH:mm");
            String sdt=dtf.format(dt);
            aidl.printText("Data da sangria: "+sdt,20,true,false,aidlUtil.AL_CENTRO);
            sdt="Term.: "+mov.getString("terminal")+" Ref.: "+mov.getString("terminal_master");
            aidl.printText(sdt,20,true,false,aidlUtil.AL_CENTRO);
            sdt="Operador:\r\n"+mov.getString("operador");
            aidl.printText(sdt,30,true,false,aidlUtil.AL_CENTRO);
            aidl.printSeparator(aidlUtil.ESEPARADOR.SEPARADOR_1);
            aidl.printText(via,20,false,false,aidlUtil.AL_ESQUERDA);
            double unitario=mov.getDouble("valor");
            String preco = NumberFormat.getCurrencyInstance(ptBr).format(unitario);
            aidl.printText(preco,40,true,false,aidlUtil.AL_CENTRO);
            aidl.lineWrap(3);
            aidl.printText("____________________\r\nAss. Operador",20,false,false,aidlUtil.AL_CENTRO);
            via="Via do parceiro";
            aidl.lineWrap(3);
          }
          callbackContext.success("ok");
        } catch (Exception ex) {
          callbackContext.error(ex.getMessage());
        }
      }
    }
    private void saleReport(String vendas,CallbackContext callbackContext){
      if (vendas == null) {
        callbackContext.error("Parametro Inválido");
      } else {
        try {
          JSONArray jv=new JSONArray(vendas);
          Locale ptBr = new Locale("pt", "BR");
          double total=0;
          aidl.printText("RELATÓRIO",40,true,true,aidlUtil.AL_CENTRO);
          Date dt=new Date();
          DateFormat dtf=new SimpleDateFormat("dd/MM/yyyy HH:mm");
          String sdt=dtf.format(dt);
          String ln="Hora Emissão: "+sdt;
          aidl.printText(ln,20,true,false,aidlUtil.AL_CENTRO);
          aidl.printSeparator(aidlUtil.ESEPARADOR.SEPARADOR_1);
          for (int i=0; i < jv.length(); i++) {
            JSONObject venda=jv.getJSONObject(i);
            aidl.printText(venda.getString("name"),30,true,false,aidlUtil.AL_ESQUERDA);
            double unitario=venda.getDouble("unitario");
            String preco = NumberFormat.getCurrencyInstance(ptBr).format(unitario);
            double totalv=venda.getDouble("sum");
            String totalStr = NumberFormat.getCurrencyInstance(ptBr).format(totalv);
            ln=preco +" x ";
            ln+=venda.getString("qtdade")+" = "+totalStr;
            aidl.printText(ln,30,false,false,aidlUtil.AL_DIREITA);
            total+=totalv;
            aidl.printSeparator(aidlUtil.ESEPARADOR.SEPARADOR_2);
          }
          String preco = NumberFormat.getCurrencyInstance(ptBr).format(total);
          ln="TOTAL: "+preco;
          aidl.printText(ln,35,true,false,aidlUtil.AL_DIREITA);
          aidl.lineWrap(5);
          callbackContext.success("ok");
        } catch (Exception ex) {
          callbackContext.error(ex.getMessage());
        }
      }

    }
    private void caixaReport(String movimento,CallbackContext callbackContext) {
      if (movimento == null) {
        callbackContext.error("Parametro Inválido");
      } else {
        try {
          JSONObject mov=new JSONObject(movimento);
          JSONObject abertura=mov.getJSONObject("abertura");
          JSONObject vendas=mov.getJSONObject("venda");
          JSONObject sangria=mov.getJSONObject("sangria");
          Locale ptBr = new Locale("pt", "BR");
          aidl.printText("FECHAMENTO DE CAIXA",40,true,true,aidlUtil.AL_CENTRO);
          Date dt=new Date();
          DateFormat dtf=new SimpleDateFormat("dd/MM/yyyy HH:mm");
          String sdt=dtf.format(dt);
          aidl.printText("Data do fechamento: "+sdt,20,true,false,aidlUtil.AL_CENTRO);
          aidl.printSeparator(aidlUtil.ESEPARADOR.SEPARADOR_1);
          double liquido=0;
          double fcaixa=abertura.getDouble("total");
          double sgria=sangria.getDouble("total");
          double tv=vendas.getDouble("total");
          String preco = NumberFormat.getCurrencyInstance(ptBr).format(fcaixa);
          aidl.printText("ABERTURA DE CAIXA (+)",30,true,false,aidlUtil.AL_ESQUERDA);
          aidl.printText(preco,30,false,false,aidlUtil.AL_DIREITA);
          aidl.printSeparator(aidlUtil.ESEPARADOR.SEPARADOR_2);
          preco = NumberFormat.getCurrencyInstance(ptBr).format(tv);
          aidl.printText("VENDAS (+)",30,true,false,aidlUtil.AL_ESQUERDA);
          aidl.printText(preco,30,false,false,aidlUtil.AL_DIREITA);
          aidl.printSeparator(aidlUtil.ESEPARADOR.SEPARADOR_2);
          preco = NumberFormat.getCurrencyInstance(ptBr).format(sgria);
          aidl.printText("SANGRIA (-)",30,true,false,aidlUtil.AL_ESQUERDA);
          aidl.printText(preco,30,false,false,aidlUtil.AL_DIREITA);
          aidl.printSeparator(aidlUtil.ESEPARADOR.SEPARADOR_2);
          liquido=fcaixa+tv-sgria;
          preco = NumberFormat.getCurrencyInstance(ptBr).format(liquido);
          String ln="TOTAL: "+preco;
          aidl.printText(ln,35,true,false,aidlUtil.AL_DIREITA);
          aidl.lineWrap(5);
          callbackContext.success("ok");

        } catch (Exception ex) {
          callbackContext.error(ex.getMessage());
        }
      }
    }

}
