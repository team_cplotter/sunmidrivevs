var exec = require('cordova/exec');

exports.coolMethod = function (arg0, success, error) {
    exec(success, error, 'SunmiPrinterVs1', 'coolMethod', [arg0]);
};
exports.toastMethod = function (arg0, success, error) {
    exec(success, error, 'SunmiPrinterVs1', 'toastMethod', [arg0]);
};
exports.printerTicket = function(arg0,success,error){
    exec(success,error,'SunmiPrinterVs1','printerTicket',[arg0]);
};
exports.printBar = function(arg0,success,error){
    exec(success,error,'SunmiPrinterVs1','printBar',[arg0]);
};
exports.startprinter=function(arg0,success,error){
    exec(success,error,'SunmiPrinterVs1','startprinter',[arg0]);
};
exports.printVoucher=function(arg0,success,error){
    exec(success,error,'SunmiPrinterVs1','printVoucher',[arg0])
};
exports.printVoucherReport=function(arg0,success,error){
    exec(success,error,'SunmiPrinterVs1','printVoucherReport',[arg0])
};
exports.abreCaixa=function(arg0,success,error){
    exec(success,error,'SunmiPrinterVs1','abreCaixa',[arg0])
};
exports.sangria=function(arg0,success,error){
    exec(success,error,'SunmiPrinterVs1','sangria',[arg0])
};
exports.saleReport=function(arg0,success,error){
    exec(success,error,'SunmiPrinterVs1','saleReport',[arg0])
};
exports.caixaReport=function(arg0,success,error){
    exec(success,error,'SunmiPrinterVs1','caixaReport',[arg0])
};